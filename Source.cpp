#include<iostream>
#include<conio.h>
#include<string>
#include<chrono>//header files i will be using in this program 
#include"termcolor.h"
using namespace std::chrono;
using namespace std;
using namespace termcolor;

void printarray();
void playermark(int count, int gamemode);
int checkvalid(int gamemode, int count);
int playerwincheck();
void restart();
int checkplayermode();
int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
string player1, player2;

int main()
{
beginning:
	{
		//choose the gamemode 
		//if one then single player , else multiplayer
		system("CLS");
		int gamemode = checkplayermode();
		int count = 1;
		int wincheck;
		double a = 1.1;
		if (gamemode == 1)
		{
			cout << cyan<<"\nEnter the player's name:";
			cin.ignore();
			getline(cin, player1);
			cout <<termcolor::reset<<magenta<< "\nYou are playing with the computer" << endl;
		}
		else if (gamemode == 2)
		{
			cout << yellow;
			cout << "\nEnter the first player's name:";
			cin.ignore();
			getline(cin, player1);
			cout << "\nEnter the second player's name:";

			getline(cin, player2);
		}


		//main game loop
		do {
			cout << green;
			printarray();
			//time measurement
			auto start = high_resolution_clock::now();
			playermark(count, gamemode);
			auto stop = high_resolution_clock::now();

			auto duration = duration_cast<milliseconds>(stop - start);

			wincheck = playerwincheck();
			count++;
			a = duration.count();

		} while (wincheck == 5);
		printarray();
		//if game ends
		if (wincheck == 0)
		{
			cout << red;
			cout << "\nYOU WON" << endl;

		}
		else if (wincheck == 1)
		{
			cout << red;
			cout << "\nIT'S A TIE" << endl;

		}
		cout << "\nExecution time taken from game start to end in milliseconds: " << a << "ms" << endl;

		cout << "\n\nTo restart the game: Enter'Y/y'" << endl;
		cout << "\nTo exit the game: Enter any other character" << endl;
		char ch;
		cin >> ch;
		if (ch == 'y' || ch == 'Y')
		{
			restart();
			goto beginning;
		}
		return 0;
	}
}
//display array
void printarray()
{

	system("CLS");
	cout << blue;
	cout << "\nTIC TAC TOE" << endl;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}


}


//game loop
void playermark(int count, int gamemode)
{

	cout << green;
	char mark;
	if (count % 2 == 1)
	{
		cout << "\n" << player1 << "It's your turn" << endl;

		mark = 0;

	}

	else if (count % 2 == 0)
	{
		if (gamemode == 1)
		{
			cout << "Computer's Turn" << endl;
		}
		if (gamemode == 2)
		{
			cout << "\n" << player2 << " ,select a position" << endl;
		}
		mark = 22;
	}

	int position = checkvalid(gamemode, count);


	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{

			if (arr[i][j] == position && (arr[i][j] != 0 || arr[i][j] != 22))
			{
				arr[i][j] = mark;

			}


		}
	}


}




//check if entered value valid or not 
int checkvalid(int gamemode, int count)
{
	int choice;

	bool playerhasquit = false;
	cout << blue;
	do
	{

		if (gamemode == 2)
		{
			cout << "Choose the position to mark:";
			cin >> choice;


		}
		if (gamemode == 1)
		{
			if (count % 2 == 1)
			{
				cout << "Choose the position to mark:";
				cin >> choice;
			}
			if (count % 2 == 0)
			{



				choice = (rand() % 8) + 1;
				cout << "Hit a key to continue" << endl;
				_getch();
			}



		}
		if (choice >= 1 && choice <= 9)
		{

			if (choice == 1 && (arr[0][0] == 0 || arr[0][0] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 2 && (arr[0][1] == 0 || arr[0][1] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 3 && (arr[0][2] == 0 || arr[0][2] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 4 && (arr[1][0] == 0 || arr[1][0] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 5 && (arr[1][1] == 0 || arr[1][1] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 6 && (arr[1][2] == 0 || arr[1][2] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 7 && (arr[2][0] == 0 || arr[2][0] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 8 && (arr[2][1] == 0 || arr[2][1] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else if (choice == 9 && (arr[2][2] == 0 || arr[2][2] == 22))
			{

				cout << "\n" << "Invalid position" << endl;
				playerhasquit = false;

			}
			else
				playerhasquit = true;
		}
		else
		{
			playerhasquit = false;
			cout << "\n" << "Invalid value:You should have entered a value from 1 to 9" << endl;
		}
	} while (!playerhasquit);

	return choice;
}
int playerwincheck()
{
	if (arr[0][0] == arr[1][0] && arr[1][0] == arr[2][0])//first column equal

		return 0;
	else if (arr[0][1] == arr[1][1] && arr[1][1] == arr[2][1])//second column equal

		return 0;
	else if (arr[0][2] == arr[1][2] && arr[1][2] == arr[2][2]) //third column equal

		return 0;
	else if (arr[0][0] == arr[0][1] && arr[0][1] == arr[0][2])  //first row equal

		return 0;
	else if (arr[1][0] == arr[1][1] && arr[1][1] == arr[1][2]) //second row equal

		return 0;
	else if (arr[2][0] == arr[2][1] && arr[2][1] == arr[2][2])//third row equal

		return 0;

	else if (arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2]) //left diagonal equal
		return 0;

	else if (arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0]) //right/primary diagonal equal
		return 0;

	//if the game is over and it is a tie
	else if (arr[0][0] != 1 && arr[0][1] != 2 && arr[0][2] != 3
		&& arr[1][0] != 4 && arr[1][1] != 5 && arr[1][2] != 6
		&& arr[2][0] != 7 && arr[2][1] != 8 && arr[2][2] != 9)

		return 1;
	else
		return  5;

}
void restart()
{
	arr[0][0] = 1;
	arr[0][1] = 2;
	arr[0][2] = 3;
	arr[1][0] = 4;
	arr[1][1] = 5;
	arr[1][2] = 6;
	arr[2][0] = 7;
	arr[2][1] = 8;
	arr[2][2] = 9;


}
int checkplayermode()
{
	int gamemode;
	cout << green;
	cout << "\nWelcome to tic tac toe" << endl;
	cout << "Choose the game mode:" << endl;
	cout << "1. Single Player" << endl;
	cout << "2.Multi-Player" << endl;
	cin >> gamemode;
	return gamemode;
}